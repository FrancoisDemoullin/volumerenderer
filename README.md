# README #

Volume Renderer / Ray Caster using C++, OpenGL and GLSL
Francois Demoullin

Find a short presentation with plenty of pretty pictures and some technical information at: [http://1drv.ms/1OZrDQF](http://1drv.ms/1OZrDQF)

### What is this repository for? ###

* This is a fully working Volume Renderer with perspective
* A volume renderer allows you to see through things, just like an X-Ray
* Starting at a simple data set (data sets can be found at volvis.org) the program produces images showing the volume using Levoy's method for Ray Casting. 


### How do I get set up? ###

Clone the repository using SourceTree onto your local machine. 
The source files come with a Microsoft Visual Studio 2015 solution file. Open the solution using Visual Studio.
Grab the external dependency file from Source Tree. This file contains all the necessary libraries and includes files. The libraries were built for a 64bit windows machine >and for all libraries the target platform is VS2015. If you are not running 64bit windows you might have to rebuild the libraries or resort to the pre-build binaries. >Information about all the necessary libraries can be found below.

Place the "Dependencies" folder such that "../../Dependencies" is accessible from Main.cpp. 
This means that you should have a folder structure similar to the following
>---------ProjectFolder  
>---------------------Dependencies  
>-------------------------------- include  
>-------------------------------- lib  
>---------------------SourceCode  
>-----------------------------ParticleSystem  
>-------------------------------------------Main.cpp  
>-------------------------------------------.cpp and .h files  
>-----------------------------VSSolutionFile 

I chose this approach of structuring my projects because I can have multiple projects referencing the dependencies using an absolute path.

### What are the parameters I can change ###

I am sorry, there is no UI to this application yet. I am working on it. 
Changing the parameters requires some code manipulation. But I am sure you are a good programmer and can figure it out :) 
If not, here are the lines you can modify.

* From Point: main.cpp line 35 (the At Point is fixed at (0, 0, 0) which is where the image is generated anyways)
* Tri-Linear interpolation is enabled by default. If you want to generate an image using tri-cubic interpolation uncomment a few lines in Raycaster. In Raycaster.cpp, comment out lines 252 + 253 and uncomment lines 255 - 257 (if you do this, expect for a LONG computation before you see any pictures). 
Once you generate this image use your keyboard to change from tri-linear to tri-cubic to the difference between the two. Press '0' for tri-linear, '1' for tri-cubic and '2' for the difference between the two.
* The resolution of a ray can be changed in Raycaster.cpp line 147 and 149.
* The color function and the opacity function are all implemented in code. That are tedious to change, however feel free to look at RayCaster.cpp lines 286 - 318. They associate a color with each value as well as an opacity. 


### Who do I talk to? ###

I know this project is not the most worked out thing in the world.
However if you did like it, please drop me a note.

Francois Demoullin
f.demoullin@gmail.com
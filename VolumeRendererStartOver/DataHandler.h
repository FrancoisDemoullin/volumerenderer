#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

struct TensorPoints
{
	float mB000;
	float mB001;
	float mB002;
	float mB003;

	float mB010;
	float mB011;
	float mB012;
	float mB013;

	float mB020;
	float mB021;
	float mB022;
	float mB023;

	float mB030;
	float mB031;
	float mB032;
	float mB033;



	float mB100;
	float mB101;
	float mB102;
	float mB103;

	float mB110;
	float mB111;
	float mB112;
	float mB113;

	float mB120;
	float mB121;
	float mB122;
	float mB123;

	float mB130;
	float mB131;
	float mB132;
	float mB133;



	float mB200;
	float mB201;
	float mB202;
	float mB203;

	float mB210;
	float mB211;
	float mB212;
	float mB213;

	float mB220;
	float mB221;
	float mB222;
	float mB223;

	float mB230;
	float mB231;
	float mB232;
	float mB233;



	float mB300;
	float mB301;
	float mB302;
	float mB303;

	float mB310;
	float mB311;
	float mB312;
	float mB313;

	float mB320;
	float mB321;
	float mB322;
	float mB323;

	float mB330;
	float mB331;
	float mB332;
	float mB333;
};

class DataHandler
{
public:
	DataHandler(char* pFileName, int pXDim, int pYDim, int pZDim);

	inline glm::vec3 getDimensions()
	{
		return glm::vec3(mXDim, mYDim, mZDim);
	}

	inline unsigned char*** getDataSet() { return mDataPoints; }
	inline glm::vec3*** getGrads() { return mGrads; }
	inline int getMaxValueInSet() { return mMaxValueInDataSet; }
	inline int getMinValueInSet() { return mMinValueInDataSet; }
	inline TensorPoints*** getTensorPoints() { return mTensorPointsArray; }

private:
	
	GLuint mDataSetTextureId;
	GLuint mGradientTextureId;

	int mXDim;
	int mYDim;
	int mZDim;
	int mTotalSize;

	int mMaxValueInDataSet;
	int mMinValueInDataSet;

	unsigned char*** mDataPoints;
	glm::vec3*** mGrads;

	TensorPoints*** mTensorPointsArray;

	void readDataSet(char * pFileName);
	void preProcessData();
	void computeGradiantBasedOnDataSet();
	void mapOpacity();
	void computeTensorPoints();

};
#pragma once

#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "Shader.h"
#include "DataHandler.h"
#include "Camera.h"

class RayCaster
{
public:
	RayCaster(int pScreenHeight, int pScreenWidth, int pDrawMode, std::vector<glm::vec3> pRayVec, DataHandler* pDH, Camera* mCamera);
	void renderTexture(int pScreenHeight, int pScreenWidth);
	void updateTexture(int pScreenHeight, int pScreenWidth);
	void generateTexture(int pScreenHeight, int pScreenWidth, std::vector<glm::vec3> pRayVec);

	void setMode(int pInput) { mDrawMode = pInput; }

private:
	GLuint mRaysTextureId;
	GLuint mScreenTextureId;
	GLuint mVAO;
	Shader* mRenderTextureShader;

	std::vector<glm::vec3> mRayVec;
	GLfloat* mRayDirectionsArrayLinear;
	GLfloat* mRayDirectionsArrayCubic;
	GLfloat* mRayDirectionsArrayDifferenceBlackWhite;

	DataHandler* mDataSet;

	Camera* mCamera;

	int mDrawMode; // 0 -> linear interpolation; 1 -> cubic interpolation; 2 -> black and white difference

	void initTexture(int pScreenHeight, int pScreenWidth);
	float linearValueInterpolation(glm::vec3 pNewPoint, float f000, float f100, float f010, float f001, float f110, float f101, float f011, float f111);
	glm::vec3 linearGradientInterpolation(glm::vec3 pNewPoint, float f000, float f100, float f010, float f001, float f110, float f101, float f011, float f111);

	float cubicValueInterpolation(TensorPoints &pTS, glm::vec3 pNewPoint);
	glm::vec3 cubicGradientInterpolation(TensorPoints &pTS, glm::vec3 pNewPoint);
	float bernsteinHelper(int pDegreeTop, int pDegreeBottom, float pValue);

	float switchHelperForGrads(TensorPoints& pTS, int xIterator, int yIterator, int zIterator, int pDirection, glm::vec3 pNewPoint);
	void normalizeBlackAndWhite(int pScreenHeight, int pScrrenWidth);
};

#pragma once
#include <glm/glm.hpp>


namespace LightingConstants
{
	extern float gIntensityAmbient;
	extern float gIntensity;
	extern float gK;
	extern float gN;
	//extern glm::vec3 gFromPoint;
	extern glm::vec3 gLightPosition;
}
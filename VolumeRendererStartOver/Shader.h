#pragma once

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <GL/glew.h>

class Shader
{
public:
	GLuint mProgram;

	Shader(const GLchar* vertexPath, const GLchar* fragmentPath);

	// copy constructor
	Shader(const Shader& pOther);

	// Uses the current shader
	void use();
};
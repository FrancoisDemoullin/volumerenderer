#pragma once

#include <vector>
#include <glm/glm.hpp>

class RayManager
{
public:

	RayManager(int pScreenWidth, int pScreenHeight);
	std::vector<glm::vec3> updateRayVector(glm::mat4 &pPerspective, glm::mat4 &pView);
	inline std::vector<glm::vec3> getRays() { return mRayVector; }
	
private:

	std::vector<glm::vec2> mRayVectorNoPerspective;
	std::vector<glm::vec3> mRayVector;

	std::vector<glm::vec2> computeRayVector(int pScreenWidth, int pScreenHeight);
	std::vector<glm::vec3> addPerspectiveToRays(std::vector<glm::vec2> &pRays, glm::mat4 &pPerspective, glm::mat4 &pView);
};
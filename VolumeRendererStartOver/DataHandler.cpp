#define _CRT_SECURE_NO_DEPRECATE

#include "DataHandler.h"

#include <fstream>
#include <iostream>
#include <vector>
using namespace std;

DataHandler::DataHandler(char* pFileName, int pXDim, int pYDim, int pZDim)
{
	mXDim = pXDim;
	mYDim = pYDim;
	mZDim = pZDim;

	mTotalSize = pXDim * pYDim * pZDim;

	mMaxValueInDataSet = 0;
	mMinValueInDataSet = 500;

	readDataSet(pFileName);
	preProcessData();
}

void DataHandler::readDataSet(char* pFileName)
{
	
	mDataPoints = new unsigned char**[mXDim];
	for (int i = 0; i < mXDim; ++i)
	{
		mDataPoints[i] = new unsigned char*[mYDim];
		for (int j = 0; j < mYDim; ++j)
		{
			mDataPoints[i][j] = new unsigned char[mZDim];
		}
	}

	ifstream ifs(pFileName, ios::binary);
	ifs.seekg(0, ios::end);
	//lSizeOfFile = ifs.tellg();
	ifs.seekg(0, ios::beg);

	for (int z = 0; z < mZDim; z++)
	{
		for (int y = 0; y < mYDim; y++)
		{
			for (int x = 0; x < mXDim; x++)
			{
				char * buffer = new char[1];
				ifs.read(buffer, 1);
				mDataPoints[x][y][z] = (unsigned char)buffer[0];

				if (mDataPoints[x][y][z] > mMaxValueInDataSet)
				{
					mMaxValueInDataSet = mDataPoints[x][y][z];
				}
				if (mDataPoints[x][y][z] < mMinValueInDataSet)
				{
					mMinValueInDataSet = mDataPoints[x][y][z];
				}
			}
		}
	}
	ifs.close();
}

void DataHandler::preProcessData()
{
	computeGradiantBasedOnDataSet();
	computeTensorPoints();
	int a = 0;
	// there is an issue with writing floating point values
	// into an array of unsigned char
	// either fix this or map opacity later using a transfer function!

	//mapOpacity();
}

void DataHandler::computeGradiantBasedOnDataSet()
{
	// create the gradiant array
	mGrads = new glm::vec3**[mXDim];
	for (int i = 0; i < mXDim; ++i)
	{
		mGrads[i] = new glm::vec3*[mYDim];
		for (int j = 0; j < mYDim; ++j)
		{
			mGrads[i][j] = new glm::vec3[mZDim];
		}
	}

	// compute the gradients
	for (int z = 0; z < mZDim; z++)
	{
		for (int y = 0; y < mYDim; y++)
		{
			for (int x = 0; x < mXDim; x++)
			{

				// x
				if (x != 0 && x != mXDim - 1)
				{
					// we are in the middle somewhere -> easy case
					mGrads[x][y][z].x = 0.5 * (mDataPoints[x - 1][y][z] - mDataPoints[x + 1][y][z]);
				}
				else if (x == 0)
				{
					mGrads[x][y][z].x = mDataPoints[x][y][z] - mDataPoints[x + 1][y][z];
				}
				else
				{
					assert(x == mXDim - 1);
					mGrads[x][y][z].x = mDataPoints[x - 1][y][z] - mDataPoints[x][y][z];
				}
				// y
				if (y != 0 && y != mYDim - 1)
				{
					// we are in the middle somewhere -> easy case
					mGrads[x][y][z].y = 0.5 * (mDataPoints[x][y - 1][z] - mDataPoints[x][y + 1][z]);
				}
				else if (y == 0)
				{
					mGrads[x][y][z].y = mDataPoints[x][y][z] - mDataPoints[x][y + 1][z];
				}
				else
				{
					assert(y == mYDim - 1);
					mGrads[x][y][z].y = mDataPoints[x][y - 1][z] - mDataPoints[x][y][z];
				}
				// z
				if (z != 0 && z != mZDim - 1)
				{
					// we are in the middle somewhere -> easy case
					mGrads[x][y][z].z = 0.5 * (mDataPoints[x][y][z - 1] - mDataPoints[x][y][z + 1]);
				}
				else if (z == 0)
				{
					mGrads[x][y][z].z = mDataPoints[x][y][z] - mDataPoints[x][y][z + 1];
				}
				else
				{
					assert(z == mZDim - 1);
					mGrads[x][y][z].z = mDataPoints[x][y][z - 1] - mDataPoints[x][y][z];
				}

				// normalize the grad at the end -> actually no, I dont think I should normalize!
				if (mGrads[x][y][z].x != 0 || mGrads[x][y][z].y != 0 || mGrads[x][y][z].z != 0)
				{
					//mGrads[x][y][z] = glm::normalize(mGrads[x][y][z]);
				}
			}
		}
	}

}

void DataHandler::mapOpacity()
{

	// map linearly for now!
	// -> 0 - 255 -> 0 - 0.8
	for (int z = 0; z < mZDim; z++)
	{
		for (int y = 0; y < mYDim; y++)
		{
			for (int x = 0; x < mXDim; x++)
			{
				mDataPoints[x][y][z] = 0.8 * (mDataPoints[x][y][z] / (mMaxValueInDataSet - mMinValueInDataSet));
			}
		}
	}
}

void DataHandler::computeTensorPoints()
{
	// create the gradiant array
	mTensorPointsArray = new TensorPoints**[mXDim];
	for (int i = 0; i < mXDim; ++i)
	{
		mTensorPointsArray[i] = new TensorPoints*[mYDim];
		for (int j = 0; j < mYDim; ++j)
		{
			mTensorPointsArray[i][j] = new TensorPoints[mZDim];
		}
	}


	// compute the tensor products
	for (int z = 0; z < mZDim - 1; z++)
	{
		for (int y = 0; y < mYDim - 1; y++)
		{
			for (int x = 0; x < mXDim - 1; x++)
			{
				mTensorPointsArray[x][y][z].mB000 = mDataPoints[x][y][z];
				mTensorPointsArray[x][y][z].mB001 = mDataPoints[x][y][z] + (1.0/3.0) * mGrads[x][y][z].z;
				mTensorPointsArray[x][y][z].mB002 = mDataPoints[x][y][z + 1] - (1.0 / 3.0) * mGrads[x][y][z + 1].z;;
				mTensorPointsArray[x][y][z].mB003 = mDataPoints[x][y][z + 1];

				mTensorPointsArray[x][y][z].mB010 = mDataPoints[x][y][z] + (1.0 / 3.0) * mGrads[x][y][z].y;
				mTensorPointsArray[x][y][z].mB011 = mDataPoints[x][y][z] + (1.0 / 3.0) * mGrads[x][y][z].y + (1.0 / 3.0) * mGrads[x][y][z].z;
				mTensorPointsArray[x][y][z].mB012 = mDataPoints[x][y][z + 1] + (1.0 / 3.0) * mGrads[x][y][z + 1].y - (1.0 / 3.0) * mGrads[x][y][z + 1].z;
				mTensorPointsArray[x][y][z].mB013 = mDataPoints[x][y][z + 1] + (1.0 / 3.0) * mGrads[x][y][z + 1].y;

				mTensorPointsArray[x][y][z].mB020 = mDataPoints[x][y + 1][z] - (1.0 / 3.0) * mGrads[x][y + 1][z].y;
				mTensorPointsArray[x][y][z].mB021 = mDataPoints[x][y + 1][z] - (1.0 / 3.0) * mGrads[x][y + 1][z].y + (1.0 / 3.0) * mGrads[x][y + 1][z].z;
				mTensorPointsArray[x][y][z].mB022 = mDataPoints[x][y + 1][z + 1] - (1.0 / 3.0) * mGrads[x][y + 1][z + 1].y - (1.0 / 3.0) * mGrads[x][y + 1][z + 1].z;
				mTensorPointsArray[x][y][z].mB023 = mDataPoints[x][y + 1][z + 1] - (1.0 / 3.0) * mGrads[x][y + 1][z + 1].y;

				mTensorPointsArray[x][y][z].mB030 = mDataPoints[x][y + 1][z];
				mTensorPointsArray[x][y][z].mB031 = mDataPoints[x][y + 1][z] + (1.0 / 3.0) * mGrads[x][y + 1][z].z;
				mTensorPointsArray[x][y][z].mB032 = mDataPoints[x][y + 1][z + 1] - (1.0 / 3.0) * mGrads[x][y + 1][z + 1].z;
				mTensorPointsArray[x][y][z].mB033 = mDataPoints[x][y + 1][z + 1];


				mTensorPointsArray[x][y][z].mB100 = mDataPoints[x][y][z] + (1.0 / 3.0) * mGrads[x][y][z].x;
				mTensorPointsArray[x][y][z].mB101 = mDataPoints[x][y][z] + (1.0 / 3.0) * mGrads[x][y][z].x + (1.0 / 3.0) * mGrads[x][y][z].z;
				mTensorPointsArray[x][y][z].mB102 = mDataPoints[x][y][z + 1] + (1.0 / 3.0) * mGrads[x][y][z + 1].x - (1.0 / 3.0) * mGrads[x][y][z + 1].z;
				mTensorPointsArray[x][y][z].mB103 = mDataPoints[x][y][z + 1] + (1.0 / 3.0) * mGrads[x][y][z + 1].x;

				mTensorPointsArray[x][y][z].mB110 = mDataPoints[x][y][z] + (1.0 / 3.0) * mGrads[x][y][z].x + (1.0 / 3.0) * mGrads[x][y][z].y;
				mTensorPointsArray[x][y][z].mB111 = mDataPoints[x][y][z] + (1.0 / 3.0) * mGrads[x][y][z].x + (1.0 / 3.0) * mGrads[x][y][z].y + (1.0 / 3.0) * mGrads[x][y][z].z;
				mTensorPointsArray[x][y][z].mB112 = mDataPoints[x][y][z + 1] + (1.0 / 3.0) * mGrads[x][y][z + 1].x + (1.0 / 3.0) * mGrads[x][y][z + 1].y - (1.0 / 3.0) * mGrads[x][y][z + 1].z;
				mTensorPointsArray[x][y][z].mB113 = mDataPoints[x][y][z + 1] + (1.0 / 3.0) * mGrads[x][y][z + 1].x + (1.0 / 3.0) * mGrads[x][y][z + 1].y;

				mTensorPointsArray[x][y][z].mB120 = mDataPoints[x][y + 1][z] + (1.0 / 3.0) * mGrads[x][y][z].x - (1.0 / 3.0) * mGrads[x][y + 1][z].y;
				mTensorPointsArray[x][y][z].mB121 = mDataPoints[x][y + 1][z] + (1.0 / 3.0) * mGrads[x][y + 1][z].x - (1.0 / 3.0) * mGrads[x][y + 1][z].y + (1.0 / 3.0) * mGrads[x][y + 1][z].z;
				mTensorPointsArray[x][y][z].mB122 = mDataPoints[x][y + 1][z + 1] + (1.0 / 3.0) * mGrads[x][y + 1][z + 1].x - (1.0 / 3.0) * mGrads[x][y + 1][z + 1].y - (1.0 / 3.0) * mGrads[x][y + 1][z + 1].z;
				mTensorPointsArray[x][y][z].mB123 = mDataPoints[x][y + 1][z + 1] + (1.0 / 3.0) * mGrads[x][y + 1][z + 1].x - (1.0 / 3.0) * mGrads[x][y + 1][z + 1].y;

				mTensorPointsArray[x][y][z].mB130 = mDataPoints[x][y + 1][z] + (1.0 / 3.0) * mGrads[x][y + 1][z].x;
				mTensorPointsArray[x][y][z].mB131 = mDataPoints[x][y + 1][z] + (1.0 / 3.0) * mGrads[x][y + 1][z].x + (1.0 / 3.0) * mGrads[x][y + 1][z].z;
				mTensorPointsArray[x][y][z].mB132 = mDataPoints[x][y + 1][z + 1] + (1.0 / 3.0) * mGrads[x][y + 1][z + 1].x - (1.0 / 3.0) * mGrads[x][y + 1][z + 1].z;
				mTensorPointsArray[x][y][z].mB133 = mDataPoints[x][y + 1][z + 1] + (1.0 / 3.0) * mGrads[x][y + 1][z + 1].x;



				mTensorPointsArray[x][y][z].mB200 = mDataPoints[x + 1][y][z] - (1.0 / 3.0) * mGrads[x + 1][y][z].x;
				mTensorPointsArray[x][y][z].mB201 = mDataPoints[x + 1][y][z] - (1.0 / 3.0) * mGrads[x + 1][y][z].x + (1.0 / 3.0) * mGrads[x + 1][y][z].z;
				mTensorPointsArray[x][y][z].mB202 = mDataPoints[x + 1][y][z + 1] - (1.0 / 3.0) * mGrads[x + 1][y][z + 1].x - (1.0 / 3.0) * mGrads[x + 1][y][z + 1].z;
				mTensorPointsArray[x][y][z].mB203 = mDataPoints[x + 1][y][z + 1] - (1.0 / 3.0) * mGrads[x + 1][y][z + 1].x;

				mTensorPointsArray[x][y][z].mB210 = mDataPoints[x + 1][y][z] - (1.0 / 3.0) * mGrads[x + 1][y][z].x + (1.0 / 3.0) * mGrads[x + 1][y][z].y;
				mTensorPointsArray[x][y][z].mB211 = mDataPoints[x + 1][y][z] - (1.0 / 3.0) * mGrads[x + 1][y][z].x + (1.0 / 3.0) * mGrads[x + 1][y][z].y + (1.0 / 3.0) * mGrads[x + 1][y][z].z;
				mTensorPointsArray[x][y][z].mB212 = mDataPoints[x + 1][y][z + 1] - (1.0 / 3.0) * mGrads[x + 1][y][z + 1].x + (1 / 3) * mGrads[x + 1][y][z + 1].y - (1 / 3) * mGrads[x + 1][y][z + 1].z;
				mTensorPointsArray[x][y][z].mB213 = mDataPoints[x + 1][y][z + 1] - (1.0 / 3.0) * mGrads[x + 1][y][z + 1].x + (1 / 3) * mGrads[x + 1][y][z + 1].y;

				mTensorPointsArray[x][y][z].mB220 = mDataPoints[x + 1][y + 1][z] - (1.0 / 3.0) * mGrads[x + 1][y + 1][z].x - (1.0 / 3.0) * mGrads[x + 1][y + 1][z].y;
				mTensorPointsArray[x][y][z].mB221 = mDataPoints[x + 1][y + 1][z] - (1.0 / 3.0) * mGrads[x + 1][y + 1][z].x - (1.0 / 3.0) * mGrads[x + 1][y + 1][z].y + (1.0 / 3.0) * mGrads[x + 1][y + 1][z].z;
				mTensorPointsArray[x][y][z].mB222 = mDataPoints[x + 1][y + 1][z + 1] - (1.0 / 3.0) * mGrads[x + 1][y + 1][z + 1].x - (1.0 / 3.0) * mGrads[x + 1][y + 1][z + 1].y - (1.0 / 3.0) * mGrads[x + 1][y + 1][z + 1].z;
				mTensorPointsArray[x][y][z].mB223 = mDataPoints[x + 1][y + 1][z + 1] - (1.0 / 3.0) * mGrads[x + 1][y + 1][z + 1].x - (1.0 / 3.0) * mGrads[x + 1][y + 1][z + 1].y;

				mTensorPointsArray[x][y][z].mB230 = mDataPoints[x + 1][y + 1][z] - (1.0 / 3.0) * mGrads[x + 1][y + 1][z].x;
				mTensorPointsArray[x][y][z].mB231 = mDataPoints[x + 1][y + 1][z] - (1.0 / 3.0) * mGrads[x + 1][y + 1][z].x + (1.0 / 3.0) * mGrads[x + 1][y + 1][z].z;
				mTensorPointsArray[x][y][z].mB232 = mDataPoints[x + 1][y + 1][z + 1] - (1.0 / 3.0) * mGrads[x + 1][y + 1][z + 1].x - (1.0 / 3.0) * mGrads[x + 1][y + 1][z + 1].z;
				mTensorPointsArray[x][y][z].mB233 = mDataPoints[x + 1][y + 1][z + 1] - (1.0 / 3.0) * mGrads[x + 1][y + 1][z + 1].x;



				mTensorPointsArray[x][y][z].mB300 = mDataPoints[x + 1][y][z];
				mTensorPointsArray[x][y][z].mB301 = mDataPoints[x + 1][y][z] + (1.0 / 3.0) * mGrads[x + 1][y][z].z;
				mTensorPointsArray[x][y][z].mB302 = mDataPoints[x + 1][y][z + 1] - (1.0 / 3.0) * mGrads[x + 1][y][z + 1].z;
				mTensorPointsArray[x][y][z].mB303 = mDataPoints[x + 1][y][z + 1];

				mTensorPointsArray[x][y][z].mB310 = mDataPoints[x + 1][y][z] + (1.0 / 3.0) * mGrads[x + 1][y][z].y;
				mTensorPointsArray[x][y][z].mB311 = mDataPoints[x + 1][y][z] + (1.0 / 3.0) * mGrads[x + 1][y][z].y + (1.0 / 3.0) * mGrads[x + 1][y][z].z;
				mTensorPointsArray[x][y][z].mB312 = mDataPoints[x + 1][y][z + 1] + (1.0 / 3.0) * mGrads[x + 1][y][z + 1].y - (1.0 / 3.0) * mGrads[x + 1][y][z + 1].z;
				mTensorPointsArray[x][y][z].mB313 = mDataPoints[x + 1][y][z + 1] + (1.0 / 3.0) * mGrads[x + 1][y][z + 1].y;

				mTensorPointsArray[x][y][z].mB320 = mDataPoints[x + 1][y + 1][z] - (1.0 / 3.0) * mGrads[x + 1][y + 1][z].y;
				mTensorPointsArray[x][y][z].mB321 = mDataPoints[x + 1][y + 1][z] - (1.0 / 3.0) * mGrads[x + 1][y + 1][z].y + (1.0 / 3.0) * mGrads[x + 1][y + 1][z].z;
				mTensorPointsArray[x][y][z].mB322 = mDataPoints[x + 1][y + 1][z + 1] - (1.0 / 3.0) * mGrads[x + 1][y + 1][z + 1].y - (1.0 / 3.0) * mGrads[x + 1][y + 1][z + 1].z;
				mTensorPointsArray[x][y][z].mB323 = mDataPoints[x + 1][y + 1][z + 1] - (1.0 / 3.0) * mGrads[x + 1][y + 1][z + 1].y;

				mTensorPointsArray[x][y][z].mB330 = mDataPoints[x + 1][y + 1][z];
				mTensorPointsArray[x][y][z].mB331 = mDataPoints[x + 1][y + 1][z] + (1.0 / 3.0) * mGrads[x + 1][y + 1][z].z;
				mTensorPointsArray[x][y][z].mB332 = mDataPoints[x + 1][y + 1][z + 1] - (1.0 / 3.0) * mGrads[x + 1][y + 1][z + 1].z;
				mTensorPointsArray[x][y][z].mB333 = mDataPoints[x + 1][y + 1][z + 1];
			}
		}
	}
}


#include "RayManager.h" 

// GLM Mathemtics

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>

RayManager::RayManager(int pScreenWidth, int pScreenHeight)
{
	mRayVectorNoPerspective = computeRayVector(pScreenWidth, pScreenHeight);
}

std::vector<glm::vec3> RayManager::updateRayVector(glm::mat4 &pPerspective, glm::mat4 &pView)
{
	// hella slow
	mRayVector = addPerspectiveToRays(mRayVectorNoPerspective, pPerspective, pView);

	// this is also a getter
	return mRayVector;
}

std::vector<glm::vec2> RayManager::computeRayVector(int pScreenWidth, int pScreenHeight)
{
	std::vector<glm::vec2> lToBeReturned;

	// compute vector
	for (int y = 0; y < pScreenWidth; y++)
	{
		for (int x = 0; x < pScreenHeight; x++)
		{
			float lX = (2.0f * x) / pScreenWidth - 1.0f;
			float lY = 1.0f - (2.0f * y) / pScreenHeight;
			float lZ = -1.0f;
			lToBeReturned.push_back(glm::vec2(lX, lY));
		}
	}
	return lToBeReturned;
}

std::vector<glm::vec3> RayManager::addPerspectiveToRays(std::vector<glm::vec2> &pRays, glm::mat4 &pPerspective, glm::mat4 &pView)
{
	glm::vec4 lTemp;
	glm::vec4 rayEyeCoords;
	glm::vec3 rayWorldCoords;

	std::vector<glm::vec3> lToBeReturned;

	for (auto const& ray : pRays)
	{
		lTemp = glm::vec4(ray.x, ray.y, -1.0, 1.0);
		rayEyeCoords = glm::affineInverse(pPerspective) * lTemp;
		rayEyeCoords = glm::vec4(rayEyeCoords.x, rayEyeCoords.y, -1.0, 0.0);
		rayEyeCoords = (glm::affineInverse(pView) * rayEyeCoords);
		rayWorldCoords = glm::vec3(rayEyeCoords.x, rayEyeCoords.y, rayEyeCoords.z);
		rayWorldCoords = glm::normalize(rayWorldCoords);

		lToBeReturned.push_back(rayWorldCoords);
	}

	return lToBeReturned;
}
#include <assert.h>
#include <cmath>

#include "Lighting.h"

#include <iostream>

using namespace std;

glm::vec3 Lighting::illuminatePoint(glm::vec3 pPoint, glm::vec3 pNormal, glm::vec3 pFromPoint, glm::vec3 pLightPosition, glm::vec3 pAmbient, glm::vec3 pDiffuse, glm::vec3 pSpecular, float pIntensityAmbient, float pIntensityLight, float pK, float pN)
{
	glm::vec3 lToBeReturned;

	// if there is no valid normal, just return black
	if (pNormal.x == 0.0 && pNormal.y == 0.0 && pNormal.z == 0.0)
	{
		return glm::vec3(0, 0, 0);
	}

	//normalize the normal vector
	float lNormalVectorLength = pow(pow(pNormal[0], 2) + pow(pNormal[1], 2) + pow(pNormal[2], 2), 0.5);
	pNormal[0] /= lNormalVectorLength;
	pNormal[1] /= lNormalVectorLength;
	pNormal[2] /= lNormalVectorLength;

	// compute the normal vector from the vertex to the light
	float lLightVectorDistance = pow(pow(pLightPosition.x - pPoint[0], 2) + pow(pLightPosition.y - pPoint[1], 2) + pow(pLightPosition.z - pPoint[2], 2), 0.5);
	float lLightVectorX = (pLightPosition.x - pPoint[0]) / lLightVectorDistance;
	float lLightVectorY = (pLightPosition.y - pPoint[1]) / lLightVectorDistance;
	float lLightVectorZ = (pLightPosition.z - pPoint[2]) / lLightVectorDistance;

	float lDotLN = lLightVectorX * pNormal[0] + lLightVectorY * pNormal[1] + lLightVectorZ * pNormal[2];

	if (lDotLN < 0.0)
	{
		lDotLN = 0.0;
	}

	// compute the normal vector from the vertex to the from point
	float lDistanceFP = pow(pow(pPoint[0] - pFromPoint.x, 2) + pow(pPoint[1] - pFromPoint.y, 2) + pow(pPoint[2] - pFromPoint.z, 2), 0.5);
	float lViewingVectorX = (pFromPoint.x - pPoint[0]) / lDistanceFP;
	float lViewingVectorY = (pFromPoint.y - pPoint[1]) / lDistanceFP;
	float lViewingVectorZ = (pFromPoint.z - pPoint[2]) / lDistanceFP;

	// compute the reflection vector using the formula from class
	float lDotVN = lViewingVectorX * pNormal[0] + lViewingVectorY * pNormal[1] + lViewingVectorZ * pNormal[2];
	if (lDotVN < 0.0)
	{
		lDotVN = 0.0;
	}

	float lReflectionX = lViewingVectorX - 2 * lDotVN * pNormal[0];
	float lReflectionY = lViewingVectorY - 2 * lDotVN * pNormal[1];
	float lReflectionZ = lViewingVectorZ - 2 * lDotVN * pNormal[2];

	float lDotRV = lReflectionX * lViewingVectorX + lReflectionY * lViewingVectorY + lReflectionZ * lViewingVectorZ;

	// finally!
	float lIntensityPointR = pAmbient.x * pIntensityAmbient + (pIntensityLight / (lDistanceFP + pK)) * (pDiffuse.x * lDotLN + pSpecular.x * pow(lDotRV, pN));
	float lIntensityPointG = pAmbient.y * pIntensityAmbient + (pIntensityLight / (lDistanceFP + pK)) * (pDiffuse.y * lDotLN + pSpecular.y * pow(lDotRV, pN));
	float lIntensityPointB = pAmbient.z * pIntensityAmbient + (pIntensityLight / (lDistanceFP + pK)) * (pDiffuse.z * lDotLN + pSpecular.z * pow(lDotRV, pN));

	float lHighestIntensity = Lighting::findMax(lIntensityPointR, lIntensityPointG, lIntensityPointB);

	// normalize
	lToBeReturned.x = (lIntensityPointR / lHighestIntensity);
	lToBeReturned.y = (lIntensityPointG / lHighestIntensity);
	lToBeReturned.z = (lIntensityPointB / lHighestIntensity);

	// et voila:
	return lToBeReturned;
}

void Lighting::normalize(vector<float>& pVector)
{
	float lDenom = 0;
	for (int i = 0; i < pVector.size(); i += 3)
	{
		lDenom = pow(pow(pVector[i], 2) + pow(pVector[i + 1], 2) + pow(pVector[i + 2], 2), 0.5);
		pVector[i] /= lDenom;
		pVector[i + 1] /= lDenom;
		pVector[i + 2] /= lDenom;
	}
}

float Lighting::findMax(float a, float b, float c)
{
	float lToBeReturned = a;
	if (b > a)
	{
		lToBeReturned = b;
	}
	if (c > lToBeReturned)
	{
		lToBeReturned = c;
	}
	return lToBeReturned;
}
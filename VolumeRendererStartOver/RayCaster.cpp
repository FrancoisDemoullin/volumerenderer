#include "RayCaster.h"
#include "Lighting.h"
#include "LightingConstants.h"


RayCaster::RayCaster(int pScreenHeight, int pScreenWidth, int pDrawMode, std::vector<glm::vec3> pRayVec, DataHandler* pDH, Camera* pCamera)
{
	GLfloat first_pass_vertices[] = {
		// Positions          // Tex 
		1.0f,  1.0f, 0.0f, 1.0f, 1.0f, // Top Right
		1.0f, -1.0f, 0.0f, 1.0f, 0.0f, // Bottom Right
		-1.0f, -1.0f, 0.0f, 0.0f, 0.0f, // Bottom Left
		-1.0f,  1.0f, 0.0f, 0.0f, 1.0f  // Top Left 
	};

	GLuint indices[] = {
		0, 1, 3, // First Triangle
		1, 2, 3  // Second Triangle
	};

	glGenVertexArrays(1, &mVAO);
	glBindVertexArray(mVAO);

	GLuint firstPassVBO;
	glGenBuffers(1, &firstPassVBO);
	glBindBuffer(GL_ARRAY_BUFFER, firstPassVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(first_pass_vertices), first_pass_vertices, GL_STATIC_DRAW);

	GLuint firstPassEBO;
	glGenBuffers(1, &firstPassEBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, firstPassEBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Textures
	initTexture(pScreenHeight, pScreenWidth);

	// array that is at the basis of the texture
	size_t lSizeOfRayArray = pScreenHeight * pScreenWidth * 3 * sizeof(GLfloat);
	mRayDirectionsArrayLinear = (GLfloat *)malloc(lSizeOfRayArray);
	mRayDirectionsArrayCubic = (GLfloat *)malloc(lSizeOfRayArray);
	mRayDirectionsArrayDifferenceBlackWhite = (GLfloat *)malloc(lSizeOfRayArray);

	mRenderTextureShader = new Shader("DebuggingVert.glsl", "DebuggingFrag.glsl");

	mDataSet = pDH;

	mCamera = pCamera;

	mDrawMode = pDrawMode;
}

void RayCaster::renderTexture(int pScreenHeight, int pScreenWidth)
{
	mRenderTextureShader->use();

	glViewport(0, 0, pScreenWidth ,pScreenHeight);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mRaysTextureId);

	glUniform1i(glGetUniformLocation(mRenderTextureShader->mProgram, "debugMe"), 0);

	glBindVertexArray(mVAO);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
}

void RayCaster::updateTexture(int pScreenHeight, int pScreenWidth)
{
	glBindTexture(GL_TEXTURE_2D, mRaysTextureId);

	if (mDrawMode == 0)
	{
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, pScreenWidth, pScreenHeight, GL_RGB, GL_FLOAT, mRayDirectionsArrayLinear);
		//glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, pScreenWidth, pScreenHeight, GL_RGB, GL_FLOAT, debuggingRED);
	}
	else if (mDrawMode == 1)
	{
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, pScreenWidth, pScreenHeight, GL_RGB, GL_FLOAT, mRayDirectionsArrayCubic);
		//glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, pScreenWidth, pScreenHeight, GL_RGB, GL_FLOAT, debuggingGREEN);
	}
	else if (mDrawMode == 2)
	{
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, pScreenWidth, pScreenHeight, GL_RGB, GL_FLOAT, mRayDirectionsArrayDifferenceBlackWhite);
		//glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, pScreenWidth, pScreenHeight, GL_RGB, GL_FLOAT, debuggingBLACK);
	}
	else
	{
		assert(1 == 0);
	}
}

void RayCaster::generateTexture(int pScreenHeight, int pScreenWidth, std::vector<glm::vec3> pRayVec)
{
	for (int i = 0; i < 3 * pScreenHeight * pScreenWidth; i++)
	{
		mRayDirectionsArrayLinear[i] = 0.0;
	}

	for (int i = 0; i < 3 * pScreenHeight * pScreenWidth; i++)
	{
		mRayDirectionsArrayCubic[i] = 0.0;
	}

	GLuint it = 0;

	glm::vec3 lDimensions = mDataSet->getDimensions();

	int lMaxDimesion = lDimensions.x;
	if (lDimensions.y > lMaxDimesion)
	{
		lMaxDimesion = lDimensions.y;
	}
	if (lDimensions.z > lMaxDimesion)
	{
		lMaxDimesion = lDimensions.z;
	}

	unsigned char *** lDataPoints = mDataSet->getDataSet();
	glm::vec3 *** lGrads = mDataSet->getGrads();


	// this is some debugging information
	int debuggingCount = 0;
	int problemCases = 0;
	int lDebuggingRed = 0;


	// traverse each ray
	while (it < pRayVec.size())
	{
		glm::vec3 temp = pRayVec[it];
		bool flag = false;
		int i;

		// 2000
		for (i = 2000; i > 0; i--)
		{
			glm::vec3 point = temp * (float)i * 0.001f + mCamera->Position; // 0.001 => much higher resolution

			// DEBUGGING

			if (point.x >= -1.0f && point.x <= 1.0f)
			{
				if (point.y >= -1.0f && point.y <= 1.0f)
				{
					if (point.z >= -1.0f && point.z <= 1.0f)
					{

						assert(-1 <= point.x); assert(point.x <= 1);
						assert(-1 <= point.y); assert(point.y <= 1);
						assert(-1 <= point.z); assert(point.z <= 1);

						// map -1..1 to max(dataSetSize)
						// we assume 1:1:1 ratio!!!!

						// lDistance of 1 voxel: from bottom right to bottom left
						float lDistanceOfVoxel = (double)2 / (lMaxDimesion - 1);

						// not sure this is correct at all!
						float lDataX = ((point.x + 1) / 2) * (lMaxDimesion - 1);
						float lDataY = ((1 - point.y) / 2) * (lMaxDimesion - 1);
						float lDataZ = ((1 - point.z) / 2) * (lMaxDimesion - 1);

						int lFloorX = (int)lDataX;
						int lFloorY = (int)lDataY;
						int lFloorZ = (int)lDataZ;

						// if floor did not do anything, meaning the point was precisely on the grid somewhere
						if (lDataX == lFloorX && lDataX != 0)
						{
							lFloorX--;
						}
						if (lDataY == lFloorY && lDataY != 0)
						{
							lFloorY--;
						}
						if (lDataZ == lFloorZ && lDataZ != 0)
						{
							lFloorZ--;
						}

						/*
							-1 + lFloorX * lDistanceOfVoxel gives me a lower bound on the voxel in world
							abs(startOfVox - point.x) gives me how far point.x is into the voxel in world coords
							map distance to 0-1 by doing distance / lDistanceOfVoxel
						*/

						float lStartOfVoxelWorldX = -1 + lFloorX * lDistanceOfVoxel;
						float lStartOfVoxelWorldY = 1 - lFloorY * lDistanceOfVoxel;
						float lStartOfVoxelWorldZ = 1 - lFloorZ * lDistanceOfVoxel;

						assert(-1 <= lStartOfVoxelWorldX); assert(lStartOfVoxelWorldX <= 1);
						assert(-1 <= lStartOfVoxelWorldY); assert(lStartOfVoxelWorldY <= 1);
						assert(-1 <= lStartOfVoxelWorldZ); assert(lStartOfVoxelWorldZ <= 1);

						float lDistanceX = glm::abs(lStartOfVoxelWorldX - point.x);
						float lDistanceY = glm::abs(lStartOfVoxelWorldY - point.y);
						float lDistanceZ = glm::abs(lStartOfVoxelWorldZ - point.z);

						assert(lDistanceX <= lDistanceOfVoxel + 0.00005); // avoid math error by making the margin bigger
						assert(lDistanceY <= lDistanceOfVoxel + 0.00005);
						assert(lDistanceZ <= lDistanceOfVoxel + 0.00005);

						glm::vec3 newPoint;

						newPoint.x = lDistanceX / lDistanceOfVoxel;
						newPoint.y = lDistanceY / lDistanceOfVoxel;
						newPoint.z = lDistanceZ / lDistanceOfVoxel;

						if (newPoint.x > 1) { newPoint.x = 1; }
						if (newPoint.y > 1) { newPoint.y = 1; }
						if (newPoint.z > 1) { newPoint.z = 1; }

						assert(0 <= newPoint.x); assert(newPoint.x <= 1);
						assert(0 <= newPoint.y); assert(newPoint.y <= 1);
						assert(0 <= newPoint.z); assert(newPoint.z <= 1);

						int f000 = lDataPoints[lFloorX][lFloorY][lFloorZ];

						int f100 = lDataPoints[lFloorX + 1][lFloorY][lFloorZ];
						int f010 = lDataPoints[lFloorX][lFloorY + 1][lFloorZ];
						int f001 = lDataPoints[lFloorX][lFloorY][lFloorZ + 1];

						int f110 = lDataPoints[lFloorX + 1][lFloorY + 1][lFloorZ];
						int f101 = lDataPoints[lFloorX + 1][lFloorY][lFloorZ + 1];
						int f011 = lDataPoints[lFloorX][lFloorY + 1][lFloorZ + 1];

						int f111 = lDataPoints[lFloorX + 1][lFloorY + 1][lFloorZ + 1];

						float lVoxelSize = (double)2.0 / (double)lMaxDimesion;


						

						float lfLinear = linearValueInterpolation(newPoint, f000, f100, f010, f001, f110, f101, f011, f111);
						glm::vec3 lGradLinear = linearGradientInterpolation(newPoint, f000, f100, f010, f001, f110, f101, f011, f111);
						
						/*For real time compilation: uncomment the next 2 lines + comment out the following 3 lines
							make sure your drawmode is on linear draw: 0, otherwise you will get a black screen
						*/
						float lfCubic = 0;
						glm::vec3 lGradCubic = glm::vec3();

						//TensorPoints lTs = mDataSet->getTensorPoints()[lFloorX][lFloorY][lFloorZ];
						//float lfCubic = cubicValueInterpolation(lTs, newPoint);
						//glm::vec3 lGradCubic = cubicGradientInterpolation(lTs, newPoint);

						// noramlize the gradients

						if (lGradLinear.x != 0 && lGradLinear.y != 0 && lGradLinear.z != 0)
						{
							lGradLinear = glm::normalize(lGradLinear);
						}
						if (lGradCubic.x != 0 && lGradCubic.y != 0 && lGradCubic.z != 0)
						{
							lGradCubic = glm::normalize(lGradCubic);
						}

						assert(mDataSet->getMaxValueInSet() - mDataSet->getMinValueInSet() != 0);
						
						float lRed = 0.0;
						float lGreen = 0.0;
						float lBlue = 0.0;

						using namespace LightingConstants;

						glm::vec3 lColorLinear;
						glm::vec3 lColorCubic;

						float lColorDependentMapHelper;

						lColorDependentMapHelper = 0.0;

						
						if (lfLinear > 100)
						{
							lColorLinear = Lighting::illuminatePoint(point, lGradLinear, mCamera->Position, LightingConstants::gLightPosition, glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.2, 0.2, 0.2), glm::vec3(0.9, 0.9, 0.9), LightingConstants::gIntensityAmbient, LightingConstants::gIntensity, LightingConstants::gK, LightingConstants::gN);
							lColorCubic = Lighting::illuminatePoint(point, lGradCubic, mCamera->Position, LightingConstants::gLightPosition, glm::vec3(0.9, 0.9, 0.9), glm::vec3(0.1, 0.1, 0.1), glm::vec3(0.8, 0.8, 0.8), LightingConstants::gIntensityAmbient, LightingConstants::gIntensity, LightingConstants::gK, LightingConstants::gN);
							
							lColorDependentMapHelper = 0.001; // temp
							//lColorLinear = glm::vec3(0.0f, 0.0f, 0.0f);
						}
						else if (lfLinear > 90)
						{
							lColorLinear = Lighting::illuminatePoint(point, lGradLinear, mCamera->Position, LightingConstants::gLightPosition, glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.6, 0.6, 0.6), glm::vec3(0.9, 0.9, 0.9), LightingConstants::gIntensityAmbient, LightingConstants::gIntensity, LightingConstants::gK, LightingConstants::gN);
							lColorCubic = Lighting::illuminatePoint(point, lGradCubic, mCamera->Position, LightingConstants::gLightPosition, glm::vec3(0.9, 0.9, 0.9), glm::vec3(0.7, 0.7, 0.7), glm::vec3(0.8, 0.8, 0.8), LightingConstants::gIntensityAmbient, LightingConstants::gIntensity, LightingConstants::gK, LightingConstants::gN);
							
							lColorDependentMapHelper = 0.05; // temp
							//lColorLinear = glm::vec3(0.0f, 0.0f, 0.0f);
						}
						else if (lfLinear > 70)
						{
							lColorLinear = Lighting::illuminatePoint(point, lGradLinear, mCamera->Position, LightingConstants::gLightPosition, glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.6, 0.6, 0.6), glm::vec3(0.9, 0.9, 0.9), LightingConstants::gIntensityAmbient, LightingConstants::gIntensity, LightingConstants::gK, LightingConstants::gN);
							lColorCubic = Lighting::illuminatePoint(point, lGradCubic, mCamera->Position, LightingConstants::gLightPosition, glm::vec3(0.9, 0.9, 0.9), glm::vec3(0.8, 0.8, 0.8), glm::vec3(0.7, 0.7, 0.7), LightingConstants::gIntensityAmbient, LightingConstants::gIntensity, LightingConstants::gK, LightingConstants::gN);

							lColorDependentMapHelper = 0.005; // temp
						}
						else if (lfLinear > 40)
						{
							lColorLinear = Lighting::illuminatePoint(point, lGradLinear, mCamera->Position, LightingConstants::gLightPosition, glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.6, 0.6, 0.6), glm::vec3(0.9, 0.9, 0.9), LightingConstants::gIntensityAmbient, LightingConstants::gIntensity, LightingConstants::gK, LightingConstants::gN);
							lColorCubic = Lighting::illuminatePoint(point, lGradCubic, mCamera->Position, LightingConstants::gLightPosition, glm::vec3(0.8, 0.8, 0.8), glm::vec3(0.4, 0.4, 0.4), glm::vec3(0.8, 0.8, 0.8), LightingConstants::gIntensityAmbient, LightingConstants::gIntensity, LightingConstants::gK, LightingConstants::gN);

							lColorDependentMapHelper = 0.0005; // temp
						}
						
						float lMappedValue = lColorDependentMapHelper * lfLinear / (mDataSet->getMaxValueInSet() - mDataSet->getMinValueInSet());
						float lMappedValueCubic = lColorDependentMapHelper * lfCubic / (mDataSet->getMaxValueInSet() - mDataSet->getMinValueInSet());

                        // Levoy's method
                        mRayDirectionsArrayLinear[3 * it] = (1 - lMappedValue) * mRayDirectionsArrayLinear[3 * it] + lMappedValue * lColorLinear.x;
						mRayDirectionsArrayLinear[3 * it + 1] = (1 - lMappedValue) * mRayDirectionsArrayLinear[3 * it + 1] + lMappedValue * lColorLinear.y;
						mRayDirectionsArrayLinear[3 * it + 2] = (1 - lMappedValue) * mRayDirectionsArrayLinear[3 * it + 2] + lMappedValue * lColorLinear.z;
						
						assert(mRayDirectionsArrayLinear[3 * it] <= 1 && mRayDirectionsArrayLinear[3 * it] >= 0);
						assert(mRayDirectionsArrayLinear[3 * it + 1] <= 1 && mRayDirectionsArrayLinear[3 * it + 1] >= 0);
						assert(mRayDirectionsArrayLinear[3 * it + 2] <= 1 && mRayDirectionsArrayLinear[3 * it + 2] >= 0);

						mRayDirectionsArrayCubic[3 * it] = (1 - lMappedValueCubic) * mRayDirectionsArrayCubic[3 * it] + lMappedValueCubic * lColorCubic.x;
						mRayDirectionsArrayCubic[3 * it + 1] = (1 - lMappedValueCubic) * mRayDirectionsArrayCubic[3 * it + 1] + lMappedValueCubic * lColorCubic.y;
						mRayDirectionsArrayCubic[3 * it + 2] = (1 - lMappedValueCubic) * mRayDirectionsArrayCubic[3 * it + 2] + lMappedValueCubic * lColorCubic.z;

						flag = true;
					}
				}
			}
		}
		it++;
	}
	std::cout << "Screen" << std::endl;
	std::cout << "DebuggingCount: " << debuggingCount << std::endl;
	
	normalizeBlackAndWhite(pScreenWidth, pScreenHeight);

	updateTexture(pScreenWidth, pScreenHeight);
}

void RayCaster::initTexture(int pScreenHeight, int pScreenWidth)
{
	glEnable(GL_TEXTURE_2D);

	glGenTextures(1, &mRaysTextureId);
	glBindTexture(GL_TEXTURE_2D, mRaysTextureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, pScreenHeight, pScreenWidth, 0, GL_RGB, GL_FLOAT, mRayDirectionsArrayLinear);
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindVertexArray(0);
}

float RayCaster::linearValueInterpolation(glm::vec3 pNewPoint, float f000, float f100, float f010, float f001, float f110, float f101, float f011, float f111 )
{
	float lf = (1 - pNewPoint.x) * (1 - pNewPoint.y) * (1 - pNewPoint.z) * f000
		+ pNewPoint.x * (1 - pNewPoint.y) * (1 - pNewPoint.z) * f100
		+ (1 - pNewPoint.x) * pNewPoint.y * (1 - pNewPoint.z) * f010
		+ (1 - pNewPoint.x) * (1 - pNewPoint.y) * pNewPoint.z * f001
		+ pNewPoint.x * pNewPoint.y * (1 - pNewPoint.z) * f110
		+ pNewPoint.x * (1 - pNewPoint.y) * pNewPoint.z * f101
		+ (1 - pNewPoint.x) * pNewPoint.y * pNewPoint.z * f011
		+ pNewPoint.x * pNewPoint.y * pNewPoint.z * f111;

	return lf;
}

glm::vec3 RayCaster::linearGradientInterpolation(glm::vec3 pNewPoint, float f000, float f100, float f010, float f001, float f110, float f101, float f011, float f111)
{
	glm::vec3 lGrad;
	// linear Gradient interpolation
	lGrad.x = -f000 + f100 + pNewPoint.y * (f000 - f100 - f010 + f110) + pNewPoint.z * (f000 - f100 - f001 + f101) + pNewPoint.z * pNewPoint.y * (-f000 + f100 + f010 + f001 - f011 - f101 - f110 + f111);
	lGrad.y = -f000 + f010 + pNewPoint.x * (f000 - f100 - f010 + f110) + pNewPoint.z * (f000 - f010 - f001 + f011) + pNewPoint.z * pNewPoint.x * (-f000 + f100 + f010 + f001 - f011 - f101 - f110 + f111);
	lGrad.z = -f000 + f001 + pNewPoint.x * (f000 - f100 - f001 + f101) + pNewPoint.y * (f000 - f010 - f001 + f011) + pNewPoint.x * pNewPoint.y * (-f000 + f100 + f010 + f001 - f011 - f101 - f110 + f111);

	return lGrad;
}

float RayCaster::cubicValueInterpolation(TensorPoints &pTS, glm::vec3 pNewPoint)
{
	float lToBeReturned = 0;

	for (int i = 0; i <= 3; i++)
	{
		for (int j = 0; j <= 3; j++)
		{
			for (int k = 0; k <= 3; k++)
			{
				switch (k)
				{
				case 0:
				{
					switch (j)
					{
					case 0:
					{
						switch (i)
						{
						case 0:
						{
							lToBeReturned += pTS.mB000 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 0 && j == 0 && k == 0);
							break;
						}
						case 1:
						{
							lToBeReturned += pTS.mB100 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 1 && j == 0 && k == 0);
							break;
						}
						case 2:
						{
							lToBeReturned += pTS.mB200 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 2 && j == 0 && k == 0);
							break;
						}
						case 3:
						{
							lToBeReturned += pTS.mB300 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 3 && j == 0 && k == 0);
							break;
						}
						default:
						{
							assert(1 == 0);
							break;
						}
						}
						break;
					}
					case 1:
					{
						switch (i)
						{
						case 0:
						{
							lToBeReturned += pTS.mB010 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 0 && j == 1 && k == 0);
							break;
						}
						case 1:
						{
							lToBeReturned += pTS.mB110 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 1 && j == 1 && k == 0);
							break;
						}
						case 2:
						{
							lToBeReturned += pTS.mB210 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 2 && j == 1 && k == 0);
							break;
						}
						case 3:
						{
							lToBeReturned += pTS.mB310 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 3 && j == 1 && k == 0);
							break;
						}
						default:
						{
							assert(1 == 0);
							break;
						}
						}
						break;
					}
					case 2:
					{
						switch (i)
						{
						case 0:
						{
							lToBeReturned += pTS.mB020 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 0 && j == 2 && k == 0);
							break;
						}
						case 1:
						{
							lToBeReturned += pTS.mB120 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 1 && j == 2 && k == 0);
							break;
						}
						case 2:
						{
							lToBeReturned += pTS.mB220 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 2 && j == 2 && k == 0);
							break;
						}
						case 3:
						{
							lToBeReturned += pTS.mB320 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 3 && j == 2 && k == 0);
							break;
						}
						default:
						{
							assert(1 == 0);
							break;
						}
						}
						break;
					}
					case 3:
					{
						switch (i)
						{
						case 0:
						{
							lToBeReturned += pTS.mB030 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 0 && j == 3 && k == 0);
							break;
						}
						case 1:
						{
							lToBeReturned += pTS.mB130 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 1 && j == 3 && k == 0);
							break;
						}
						case 2:
						{
							lToBeReturned += pTS.mB230 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 2 && j == 3 && k == 0);
							break;
						}
						case 3:
						{
							lToBeReturned += pTS.mB330 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 3 && j == 3 && k == 0);
							break;
						}
						default:
						{
							assert(1 == 0);
							break;
						}
						}
						break;
					}
					default:
					{
						assert(1 == 0);
						break;
					}
					}
					break;
				}
				case 1:
				{
					switch (j)
					{
					case 0:
					{
						switch (i)
						{
						case 0:
						{
							lToBeReturned += pTS.mB001 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 0 && j == 0 && k == 1);
							break;
						}
						case 1:
						{
							lToBeReturned += pTS.mB101 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 1 && j == 0 && k == 1);
							break;
						}
						case 2:
						{
							lToBeReturned += pTS.mB201 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 2 && j == 0 && k == 1);
							break;
						}
						case 3:
						{
							lToBeReturned += pTS.mB301 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 3 && j == 0 && k == 1);
							break;
						}
						default:
						{
							assert(1 == 0);
							break;
						}
						}
						break;
					}
					case 1:
					{
						switch (i)
						{
						case 0:
						{
							lToBeReturned += pTS.mB011 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 0 && j == 1 && k == 1);
							break;
						}
						case 1:
						{
							lToBeReturned += pTS.mB111 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 1 && j == 1 && k == 1);
							break;
						}
						case 2:
						{
							lToBeReturned += pTS.mB211 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 2 && j == 1 && k == 1);
							break;
						}
						case 3:
						{
							lToBeReturned += pTS.mB311 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 3 && j == 1 && k == 1);
							break;
						}
						default:
						{
							assert(1 == 0);
							break;
						}
						}
						break;
					}
					case 2:
					{
						switch (i)
						{
						case 0:
						{
							lToBeReturned += pTS.mB021 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 0 && j == 2 && k == 1);
							break;
						}
						case 1:
						{
							lToBeReturned += pTS.mB121 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 1 && j == 2 && k == 1);
							break;
						}
						case 2:
						{
							lToBeReturned += pTS.mB221 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 2 && j == 2 && k == 1);
							break;
						}
						case 3:
						{
							lToBeReturned += pTS.mB321 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 3 && j == 2 && k == 1);
							break;
						}
						default:
						{
							assert(1 == 0);
							break;
						}
						}
						break;
					}
					case 3:
					{
						switch (i)
						{
						case 0:
						{
							lToBeReturned += pTS.mB031 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 0 && j == 3 && k == 1);
							break;
						}
						case 1:
						{
							lToBeReturned += pTS.mB131 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 1 && j == 3 && k == 1);
							break;
						}
						case 2:
						{
							lToBeReturned += pTS.mB231 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 2 && j == 3 && k == 1);
							break;
						}
						case 3:
						{
							lToBeReturned += pTS.mB331 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 3 && j == 3 && k == 1);
							break;
						}
						default:
						{
							assert(1 == 0);
							break;
						}
						}
						break;
					}
					default:
					{
						assert(1 == 0);
						break;
					}
					}
					break;
				}
				case 2:
				{
					switch (j)
					{
					case 0:
					{
						switch (i)
						{
						case 0:
						{
							lToBeReturned += pTS.mB002 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 0 && j == 0 && k == 2);
							break;
						}
						case 1:
						{
							lToBeReturned += pTS.mB102 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 1 && j == 0 && k == 2);
							break;
						}
						case 2:
						{
							lToBeReturned += pTS.mB202 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 2 && j == 0 && k == 2);
							break;
						}
						case 3:
						{
							lToBeReturned += pTS.mB302 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 3 && j == 0 && k == 2);
							break;
						}
						default:
						{
							assert(1 == 0);
							break;
						}
						}
						break;
					}
					case 1:
					{
						switch (i)
						{
						case 0:
						{
							lToBeReturned += pTS.mB012 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 0 && j == 1 && k == 2);
							break;
						}
						case 1:
						{
							lToBeReturned += pTS.mB112 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 1 && j == 1 && k == 2);
							break;
						}
						case 2:
						{
							lToBeReturned += pTS.mB212 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 2 && j == 1 && k == 2);
							break;
						}
						case 3:
						{
							lToBeReturned += pTS.mB312 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 3 && j == 1 && k == 2);
							break;
						}
						default:
						{
							assert(1 == 0);
							break;
						}
						}
						break;
					}
					case 2:
					{
						switch (i)
						{
						case 0:
						{
							lToBeReturned += pTS.mB022 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 0 && j == 2 && k == 2);
							break;
						}
						case 1:
						{
							lToBeReturned += pTS.mB122 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 1 && j == 2 && k == 2);
							break;
						}
						case 2:
						{
							lToBeReturned += pTS.mB222 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 2 && j == 2 && k == 2);
							break;
						}
						case 3:
						{
							lToBeReturned += pTS.mB322 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 3 && j == 2 && k == 2);
							break;
						}
						default:
						{
							assert(1 == 0);
							break;
						}
						}
						break;
					}
					case 3:
					{
						switch (i)
						{
						case 0:
						{
							lToBeReturned += pTS.mB032 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 0 && j == 3 && k == 2);
							break;
						}
						case 1:
						{
							lToBeReturned += pTS.mB132 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 1 && j == 3 && k == 2);
							break;
						}
						case 2:
						{
							lToBeReturned += pTS.mB232 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 2 && j == 3 && k == 2);
							break;
						}
						case 3:
						{
							lToBeReturned += pTS.mB332 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 3 && j == 3 && k == 2);
							break;
						}
						default:
						{
							assert(1 == 0);
							break;
						}
						}
						break;
					}
					default:
					{
						assert(1 == 0);
						break;
					}
					}
					break;
				}
				case 3:
				{
					switch (j)
					{
					case 0:
					{
						switch (i)
						{
						case 0:
						{
							lToBeReturned += pTS.mB003 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 0 && j == 0 && k == 3);
							break;
						}
						case 1:
						{
							lToBeReturned += pTS.mB103 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 1 && j == 0 && k == 3);
							break;
						}
						case 2:
						{
							lToBeReturned += pTS.mB203 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 2 && j == 0 && k == 3);
							break;
						}
						case 3:
						{
							lToBeReturned += pTS.mB303 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 3 && j == 0 && k == 3);
							break;
						}
						default:
						{
							assert(1 == 0);
							break;
						}
						}
						break;
					}
					case 1:
					{
						switch (i)
						{
						case 0:
						{
							lToBeReturned += pTS.mB013 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 0 && j == 1 && k == 3);
							break;
						}
						case 1:
						{
							lToBeReturned += pTS.mB113 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 1 && j == 1 && k == 3);
							break;
						}
						case 2:
						{
							lToBeReturned += pTS.mB213 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 2 && j == 1 && k == 3);
							break;
						}
						case 3:
						{
							lToBeReturned += pTS.mB313 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 3 && j == 1 && k == 3);
							break;
						}
						default:
						{
							assert(1 == 0);
							break;
						}
						}
						break;
					}
					case 2:
					{
						switch (i)
						{
						case 0:
						{
							lToBeReturned += pTS.mB023 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 0 && j == 2 && k == 3);
							break;
						}
						case 1:
						{
							lToBeReturned += pTS.mB123 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 1 && j == 2 && k == 3);
							break;
						}
						case 2:
						{
							lToBeReturned += pTS.mB223 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 2 && j == 2 && k == 3);
							break;
						}
						case 3:
						{
							lToBeReturned += pTS.mB323 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 3 && j == 2 && k == 3);
							break;
						}
						default:
						{
							assert(1 == 0);
							break;
						}
						}
						break;
					}
					case 3:
					{
						switch (i)
						{
						case 0:
						{
							lToBeReturned += pTS.mB033 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 0 && j == 3 && k == 3);
							break;
						}
						case 1:
						{
							lToBeReturned += pTS.mB133 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 1 && j == 3 && k == 3);
							break;
						}
						case 2:
						{
							lToBeReturned += pTS.mB233 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 2 && j == 3 && k == 3);
							break;
						}
						case 3:
						{
							lToBeReturned += pTS.mB333 * bernsteinHelper(3, i, pNewPoint.x) * bernsteinHelper(3, j, pNewPoint.y) * bernsteinHelper(3, k, pNewPoint.z);
							assert(i == 3 && j == 3 && k == 3);
							break;
						}
						default:
						{
							assert(1 == 0);
							break;
						}
						}
						break;
					}
					default:
					{
						assert(1 == 0);
						break;
					}
					}
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				} // end of k switch statement

			} // end of for loop
		} // end of for loop
	} // end of for loop

	return lToBeReturned;
}

glm::vec3 RayCaster::cubicGradientInterpolation(TensorPoints & pTS, glm::vec3 pNewPoint)
{
	glm::vec3 lToBeReturned;

	float lToBeReturnedX = 0;
	float lToBeReturnedY = 0;
	float lToBeReturnedZ = 0;

	for (int xIterator = 0; xIterator <= 3; xIterator++)
	{
		for (int yIterator = 0; yIterator <= 3; yIterator++)
		{
			for (int zIterator = 0; zIterator <= 3; zIterator++)
			{
				if (xIterator != 3)
				{
					lToBeReturnedX += switchHelperForGrads(pTS, xIterator, yIterator, zIterator, 0, pNewPoint);
				}
				if (yIterator != 3)
				{
					lToBeReturnedY += switchHelperForGrads(pTS, xIterator, yIterator, zIterator, 1, pNewPoint);
				}
				if (zIterator != 3)
				{
					lToBeReturnedZ += switchHelperForGrads(pTS, xIterator, yIterator, zIterator, 2, pNewPoint);
				}

			} // end of for loop
		} // end of for loop
	} // end of for loop

	lToBeReturned = glm::vec3(lToBeReturnedX, lToBeReturnedY, lToBeReturnedZ);
	lToBeReturned *= 3;
	return lToBeReturned;
}


float RayCaster::bernsteinHelper(int pDegreeTop, int pDegreeBottom, float pValue)
{
	float lToBeReturned;
	if (pDegreeTop == 3)
	{
		switch (pDegreeBottom)
		{
		case 0:
			lToBeReturned = (1 - pValue) * (1 - pValue) * (1 - pValue);
			break;
		case 1:
			lToBeReturned = 3 * (1 - pValue) * (1 - pValue) * pValue;
			break;
		case 2:
			lToBeReturned = 3 * (1 - pValue) * pValue * pValue;
			break;
		case 3:
			lToBeReturned = pValue * pValue * pValue;
			break;
		default:
			assert(1 == 0);
			break;
		}
	}
	else if (pDegreeTop == 2)
	{
		switch (pDegreeBottom)
		{
		case 0:
			lToBeReturned = (1 - pValue) * (1 - pValue);
			break;
		case 1:
			lToBeReturned = 2 * (1 - pValue) * pValue;
			break;
		case 2:
			lToBeReturned = pValue * pValue;
			break;
		default:
			assert(1 == 0);
			break;
		}
	}
	else
	{
		assert(1 == 0);
	}
	
	return lToBeReturned;
}

float RayCaster::switchHelperForGrads(TensorPoints& pTS, int xIterator, int yIterator, int zIterator, int pDirection, glm::vec3 pNewPoint)
{
	float lToBeReturned = 0.0;

	int lXBernsteinIndex;
	int lYBernsteinIndex;
	int lZBernsteinIndex;

	if (pDirection == 0) // we compute the X component
	{
		lXBernsteinIndex = 2;
		lYBernsteinIndex = 3;
		lZBernsteinIndex = 3;

		switch (zIterator)
		{
		case 0:
		{
			switch (yIterator)
			{
			case 0:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB100 - pTS.mB000) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 0 && zIterator == 0);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB200 - pTS.mB100) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 0 && zIterator == 0);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB200 - pTS.mB300) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 0 && zIterator == 0);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 1:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB110 - pTS.mB010) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 1 && zIterator == 0);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB210 - pTS.mB110) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 1 && zIterator == 0);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB310 - pTS.mB210) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 1 && zIterator == 0);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 2:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB120 - pTS.mB020) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 2 && zIterator == 0);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB220 - pTS.mB120) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 2 && zIterator == 0);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB320 - pTS.mB220) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 2 && zIterator == 0);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 3:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB130 - pTS.mB030) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 3 && zIterator == 0);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB230 - pTS.mB130) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 3 && zIterator == 0);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB330 - pTS.mB230) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 3 && zIterator == 0);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			default:
			{
				assert(1 == 0);
				break;
			}
			}
			break;
		}
		case 1:
		{
			switch (yIterator)
			{
			case 0:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB101 - pTS.mB001) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 0 && zIterator == 1);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB201 - pTS.mB101) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 0 && zIterator == 1);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB301 - pTS.mB201) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 0 && zIterator == 1);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 1:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB111 - pTS.mB011) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 1 && zIterator == 1);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB211 - pTS.mB111) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 1 && zIterator == 1);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB311 - pTS.mB211) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 1 && zIterator == 1);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 2:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB121 - pTS.mB021) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 2 && zIterator == 1);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB221 - pTS.mB121) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 2 && zIterator == 1);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB321 - pTS.mB221) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 2 && zIterator == 1);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 3:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB131 - pTS.mB031) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 3 && zIterator == 1);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB231 - pTS.mB131) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 3 && zIterator == 1);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB331 - pTS.mB231) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 3 && zIterator == 1);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			default:
			{
				assert(1 == 0);
				break;
			}
			}
			break;
		}
		case 2:
		{
			switch (yIterator)
			{
			case 0:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB102 - pTS.mB002) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 0 && zIterator == 2);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB202 - pTS.mB102) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 0 && zIterator == 2);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB302 - pTS.mB202) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 0 && zIterator == 2);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 1:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB112 - pTS.mB012) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 1 && zIterator == 2);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB212 - pTS.mB112) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 1 && zIterator == 2);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB312 - pTS.mB212) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 1 && zIterator == 2);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 2:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB122 - pTS.mB022) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 2 && zIterator == 2);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB222 - pTS.mB122) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 2 && zIterator == 2);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB322 - pTS.mB222) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 2 && zIterator == 2);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 3:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB132 - pTS.mB032) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 3 && zIterator == 2);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB232 - pTS.mB132) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 3 && zIterator == 2);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB332 - pTS.mB232) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 3 && zIterator == 2);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			default:
			{
				assert(1 == 0);
				break;
			}
			}
			break;
		}
		case 3:
		{
			switch (yIterator)
			{
			case 0:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB103 - pTS.mB003) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 0 && zIterator == 3);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB203 - pTS.mB103) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 0 && zIterator == 3);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB303 - pTS.mB203) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 0 && zIterator == 3);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 1:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB113 - pTS.mB013) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 1 && zIterator == 3);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB213 - pTS.mB113) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 1 && zIterator == 3);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB313 - pTS.mB213) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 1 && zIterator == 3);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 2:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB123 - pTS.mB023) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 2 && zIterator == 3);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB223 - pTS.mB123) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 2 && zIterator == 3);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB323 - pTS.mB223) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 2 && zIterator == 3);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 3:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB133 - pTS.mB033) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 3 && zIterator == 3);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB233 - pTS.mB133) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 3 && zIterator == 3);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB333 - pTS.mB233) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 3 && zIterator == 3);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			default:
			{
				assert(1 == 0);
				break;
			}
			}
			break;
		}
		default:
		{
			assert(1 == 0);
			break;
		}
		} // end of k switch statement
	} // end of x if statement
	if (pDirection == 1) // now we compute the Y component
	{
		lXBernsteinIndex = 3;
		lYBernsteinIndex = 2;
		lZBernsteinIndex = 3;

		switch (zIterator)
		{
		case 0:
		{
			switch (yIterator)
			{
			case 0:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB010 - pTS.mB000) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 0 && zIterator == 0);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB110 - pTS.mB100) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 0 && zIterator == 0);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB210 - pTS.mB200) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 0 && zIterator == 0);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB310 - pTS.mB300) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 0 && zIterator == 0);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 1:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB020 - pTS.mB010) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 1 && zIterator == 0);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB120 - pTS.mB110) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 1 && zIterator == 0);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB220 - pTS.mB210) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 1 && zIterator == 0);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB320 - pTS.mB310) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 1 && zIterator == 0);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 2:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB030 - pTS.mB020) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 2 && zIterator == 0);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB130 - pTS.mB120) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 2 && zIterator == 0);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB230 - pTS.mB220) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 2 && zIterator == 0);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB330 - pTS.mB320) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 2 && zIterator == 0);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 3:
			{
				assert(1 == 0);
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB130 - pTS.mB030) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 3 && zIterator == 0);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB230 - pTS.mB130) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 3 && zIterator == 0);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB330 - pTS.mB230) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 3 && zIterator == 0);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			default:
			{
				assert(1 == 0);
				break;
			}
			}
			break;
		}
		case 1:
		{
			switch (yIterator)
			{
			case 0:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB011 - pTS.mB001) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 0 && zIterator == 1);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB111 - pTS.mB101) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 0 && zIterator == 1);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB211 - pTS.mB201) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 0 && zIterator == 1);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB311 - pTS.mB301) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 0 && zIterator == 1);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 1:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB021 - pTS.mB011) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 1 && zIterator == 1);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB121 - pTS.mB111) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 1 && zIterator == 1);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB221 - pTS.mB211) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 1 && zIterator == 1);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB321 - pTS.mB311) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 1 && zIterator == 1);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 2:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB031 - pTS.mB021) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 2 && zIterator == 1);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB131 - pTS.mB121) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 2 && zIterator == 1);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB231 - pTS.mB221) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 2 && zIterator == 1);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB331 - pTS.mB321) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 2 && zIterator == 1);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 3:
			{
				assert(1 == 0);
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB131 - pTS.mB031) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 3 && zIterator == 1);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB231 - pTS.mB131) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 3 && zIterator == 1);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB331 - pTS.mB231) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 3 && zIterator == 1);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			default:
			{
				assert(1 == 0);
				break;
			}
			}
			break;
		}
		case 2:
		{
			switch (yIterator)
			{
			case 0:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB012 - pTS.mB002) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 0 && zIterator == 2);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB112 - pTS.mB102) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 0 && zIterator == 2);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB212 - pTS.mB202) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 0 && zIterator == 2);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB312 - pTS.mB302) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 0 && zIterator == 2);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 1:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB022 - pTS.mB012) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 1 && zIterator == 2);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB122 - pTS.mB112) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 1 && zIterator == 2);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB222 - pTS.mB212) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 1 && zIterator == 2);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB322 - pTS.mB312) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 1 && zIterator == 2);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 2:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB032 - pTS.mB022) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 2 && zIterator == 2);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB132 - pTS.mB122) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 2 && zIterator == 2);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB232 - pTS.mB222) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 2 && zIterator == 2);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB332 - pTS.mB322) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 2 && zIterator == 2);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 3:
			{
				assert(1 == 0);
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB132 - pTS.mB032) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 3 && zIterator == 2);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB232 - pTS.mB132) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 3 && zIterator == 2);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB332 - pTS.mB232) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 3 && zIterator == 2);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			default:
			{
				assert(1 == 0);
				break;
			}
			}
			break;
		}
		case 3:
		{
			switch (yIterator)
			{
			case 0:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB013 - pTS.mB003) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 0 && zIterator == 3);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB113 - pTS.mB103) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 0 && zIterator == 3);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB213 - pTS.mB203) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 0 && zIterator == 3);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB313 - pTS.mB303) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 0 && zIterator == 3);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 1:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB023 - pTS.mB013) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 1 && zIterator == 3);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB123 - pTS.mB113) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 1 && zIterator == 3);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB223 - pTS.mB213) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 1 && zIterator == 3);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB323 - pTS.mB313) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 1 && zIterator == 3);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 2:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB033 - pTS.mB023) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 2 && zIterator == 3);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB133 - pTS.mB123) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 2 && zIterator == 3);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB233 - pTS.mB223) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 2 && zIterator == 3);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB333 - pTS.mB323) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 2 && zIterator == 3);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 3:
			{
				assert(1 == 0);
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB133 - pTS.mB033) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 3 && zIterator == 3);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB233 - pTS.mB133) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 3 && zIterator == 3);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB333 - pTS.mB233) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 3 && zIterator == 3);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			default:
			{
				assert(1 == 0);
				break;
			}
			}
			break;
		}
		default:
		{
			assert(1 == 0);
			break;
		}
		} // end of k switch statement
	} // end of y if statement
	if (pDirection == 2) // finally we compute the Z component
	{
		lXBernsteinIndex = 3;
		lYBernsteinIndex = 3;
		lZBernsteinIndex = 2;

		switch (zIterator)
		{
		case 0:
		{
			switch (yIterator)
			{
			case 0:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB001 - pTS.mB000) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 0 && zIterator == 0);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB101 - pTS.mB100) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 0 && zIterator == 0);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB201 - pTS.mB200) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 0 && zIterator == 0);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB301 - pTS.mB300) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 0 && zIterator == 0);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 1:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB011 - pTS.mB010) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 1 && zIterator == 0);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB111 - pTS.mB110) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 1 && zIterator == 0);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB211 - pTS.mB210) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 1 && zIterator == 0);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB311 - pTS.mB310) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 1 && zIterator == 0);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 2:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB021 - pTS.mB020) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 2 && zIterator == 0);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB121 - pTS.mB120) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 2 && zIterator == 0);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB221 - pTS.mB220) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 2 && zIterator == 0);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB321 - pTS.mB320) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 2 && zIterator == 0);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 3:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB031 - pTS.mB030) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 3 && zIterator == 0);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB131 - pTS.mB130) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 3 && zIterator == 0);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB231 - pTS.mB230) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 3 && zIterator == 0);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB331 - pTS.mB330) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 3 && zIterator == 0);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			default:
			{
				assert(1 == 0);
				break;
			}
			}
			break;
		}
		case 1:
		{
			switch (yIterator)
			{
			case 0:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB002 - pTS.mB001) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 0 && zIterator == 1);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB102 - pTS.mB101) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 0 && zIterator == 1);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB202 - pTS.mB201) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 0 && zIterator == 1);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB302 - pTS.mB301) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 0 && zIterator == 1);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 1:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB012 - pTS.mB011) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 1 && zIterator == 1);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB112 - pTS.mB111) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 1 && zIterator == 1);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB212 - pTS.mB211) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 1 && zIterator == 1);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB312 - pTS.mB311) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 1 && zIterator == 1);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 2:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB022 - pTS.mB021) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 2 && zIterator == 1);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB122 - pTS.mB121) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 2 && zIterator == 1);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB222 - pTS.mB221) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 2 && zIterator == 1);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB322 - pTS.mB321) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 2 && zIterator == 1);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 3:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB032 - pTS.mB031) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 3 && zIterator == 1);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB132 - pTS.mB131) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 3 && zIterator == 1);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB232 - pTS.mB231) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 3 && zIterator == 1);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB332 - pTS.mB331) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 3 && zIterator == 1);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			default:
			{
				assert(1 == 0);
				break;
			}
			}
			break;
		}
		case 2:
		{
			switch (yIterator)
			{
			case 0:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB003 - pTS.mB002) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 0 && zIterator == 2);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB103 - pTS.mB102) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 0 && zIterator == 2);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB203 - pTS.mB202) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 0 && zIterator == 2);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB303 - pTS.mB302) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 0 && zIterator == 2);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 1:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB013 - pTS.mB012) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 1 && zIterator == 2);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB113 - pTS.mB112) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 1 && zIterator == 2);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB213 - pTS.mB212) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 1 && zIterator == 2);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB313 - pTS.mB312) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 1 && zIterator == 2);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 2:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB023 - pTS.mB022) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 2 && zIterator == 2);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB123 - pTS.mB122) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 2 && zIterator == 2);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB223 - pTS.mB222) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 2 && zIterator == 2);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB323 - pTS.mB322) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 2 && zIterator == 2);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 3:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB033 - pTS.mB032) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 3 && zIterator == 2);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB133 - pTS.mB132) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 3 && zIterator == 2);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB233 - pTS.mB232) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 3 && zIterator == 2);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB333 - pTS.mB332) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 3 && zIterator == 2);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			default:
			{
				assert(1 == 0);
				break;
			}
			}
			break;
		}
		case 3:
		{
			assert(1 == 0);
			switch (yIterator)
			{
			case 0:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB013 - pTS.mB003) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 0 && zIterator == 3);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB113 - pTS.mB103) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 0 && zIterator == 3);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB213 - pTS.mB203) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 0 && zIterator == 3);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB313 - pTS.mB303) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 0 && zIterator == 3);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 1:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB023 - pTS.mB013) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 1 && zIterator == 3);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB123 - pTS.mB113) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 1 && zIterator == 3);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB223 - pTS.mB213) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 1 && zIterator == 3);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB323 - pTS.mB313) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 1 && zIterator == 3);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 2:
			{
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB033 - pTS.mB023) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 2 && zIterator == 3);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB133 - pTS.mB123) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 2 && zIterator == 3);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB233 - pTS.mB223) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 2 && zIterator == 3);
					break;
				}
				case 3:
				{
					lToBeReturned = (pTS.mB333 - pTS.mB323) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 3 && yIterator == 2 && zIterator == 3);
					break;
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			case 3:
			{
				assert(1 == 0);
				switch (xIterator)
				{
				case 0:
				{
					lToBeReturned = (pTS.mB133 - pTS.mB033) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 0 && yIterator == 3 && zIterator == 3);
					break;
				}
				case 1:
				{
					lToBeReturned = (pTS.mB233 - pTS.mB133) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 1 && yIterator == 3 && zIterator == 3);
					break;
				}
				case 2:
				{
					lToBeReturned = (pTS.mB333 - pTS.mB233) * bernsteinHelper(lXBernsteinIndex, xIterator, pNewPoint.x) * bernsteinHelper(lYBernsteinIndex, yIterator, pNewPoint.y) * bernsteinHelper(lZBernsteinIndex, zIterator, pNewPoint.z);
					assert(xIterator == 2 && yIterator == 3 && zIterator == 3);
					break;
				}
				case 3:
				{
					assert(1 == 0);
				}
				default:
				{
					assert(1 == 0);
					break;
				}
				}
				break;
			}
			default:
			{
				assert(1 == 0);
				break;
			}
			}
			break;
		}
		default:
		{
			assert(1 == 0);
			break;
		}
		} // end of k switch statement
	} // end of y if statement

	return lToBeReturned;
}

void RayCaster::normalizeBlackAndWhite(int pScreenHeight, int pScrrenWidth)
{
	float differenceValue = 0.0;

	float lMin = mDataSet->getMaxValueInSet();
	float lMax = mDataSet->getMinValueInSet();

	for (int i = 0; i < 3 * pScreenHeight * pScrrenWidth; i++)
	{
		mRayDirectionsArrayDifferenceBlackWhite[i] = 0.0;
	}


	int lDebuggingHelper = 0;
	for (int it = 0; it < pScreenHeight * pScrrenWidth; it++)
	{
		differenceValue = abs(mRayDirectionsArrayLinear[3 * it] - mRayDirectionsArrayCubic[3 * it]) + abs(mRayDirectionsArrayLinear[3 * it + 1] - mRayDirectionsArrayCubic[3 * it + 1]) + abs(mRayDirectionsArrayLinear[3 * it + 2] - mRayDirectionsArrayCubic[3 * it + 2]);

		if (differenceValue != 0.0)
		{
			lDebuggingHelper++;
		}

		if (differenceValue > lMax)
		{
			lMax = differenceValue;
		}
		if (differenceValue < lMin)
		{
			lMin = differenceValue;
		}

		mRayDirectionsArrayDifferenceBlackWhite[3 * it] = differenceValue;
		mRayDirectionsArrayDifferenceBlackWhite[3 * it + 1] = differenceValue;
		mRayDirectionsArrayDifferenceBlackWhite[3 * it + 2] = differenceValue;
	}

	//assert(lMin < lMax);

	for (int it = 0; it < pScreenHeight * pScrrenWidth; it++)
	{
		mRayDirectionsArrayDifferenceBlackWhite[3 * it] =  (float)(mRayDirectionsArrayDifferenceBlackWhite[3 * it] - lMin) / (float)(lMax - lMin);
		mRayDirectionsArrayDifferenceBlackWhite[3 * it + 1] = (float)(mRayDirectionsArrayDifferenceBlackWhite[3 * it + 1] - lMin) / (float)(lMax - lMin);
		mRayDirectionsArrayDifferenceBlackWhite[3 * it + 2] =  (float)(mRayDirectionsArrayDifferenceBlackWhite[3 * it + 2] - lMin) / (float)(lMax - lMin);
	}
	std::cout << "Difference was not 0 at: " << lDebuggingHelper << " points." << std::endl;
}

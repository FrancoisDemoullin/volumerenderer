#pragma once

#include <vector>
#include <glm/glm.hpp>

using namespace std;

class Lighting
{
public:
	static glm::vec3 Lighting::illuminatePoint(glm::vec3 pPoint, glm::vec3 pNormal, glm::vec3 pFromPoint, glm::vec3 pLightPosition, glm::vec3 pAmbient, glm::vec3 pDiffuse, glm::vec3 pSpecular, float pIntensityAmbient, float pIntensityLight, float pK, float pN);
	static void normalize(vector<float>& pVector);

	static float findMax(float a, float b, float c);
};
#version 430

in vec2 UV;

out vec4 color;

uniform sampler2D debugMe;

void main()
{
	vec4 fromTexture = texture(debugMe, UV);
	color = fromTexture;
}